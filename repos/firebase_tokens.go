package repos

import (
	"encoding/json"
	"errors"
	"github.com/syndtr/goleveldb/leveldb"
)

type h map[string]interface{}

type FirebaseTokens struct {
	key   string
	level *leveldb.DB
}

func NewFirebaseTokens(fullFilepath string) (ft *FirebaseTokens, err error) {
	db, _err := leveldb.OpenFile(fullFilepath, nil)
	if _err != nil {
		err = _err
		return
	}

	ft = &FirebaseTokens{
		key:   "ft",
		level: db,
	}

	return
}

func (ft *FirebaseTokens) GetAll() (tokens []string, err error) {
	var old h
	oldBytes, readErr := ft.level.Get([]byte(ft.key), nil)
	if readErr == leveldb.ErrNotFound {
		return
	} else if readErr != nil {
		err = readErr
		return
	}

	err = json.Unmarshal(oldBytes, &old)
	if err != nil {
		return
	}

	interfaces, ok := old["tokens"].([]interface{})
	if !ok {
		err = errors.New("invalid structure inside db")
	}

	tokens = make([]string, len(interfaces))
	for i := range tokens {
		tokens[i], ok = interfaces[i].(string)
		if !ok {
			err = errors.New("invalid structure inside db")
		}
	}

	return
}

func (ft *FirebaseTokens) Add(token string) (err error) {
	var old h
	oldBytes, readErr := ft.level.Get([]byte(ft.key), nil)
	if readErr == nil {
		err = json.Unmarshal(oldBytes, &old)
		if err != nil {
			return
		}
	} else if readErr == leveldb.ErrNotFound {
		old = h{"tokens": make([]interface{}, 0)}
	} else {
		err = readErr
		return
	}

	tokens, ok := old["tokens"].([]interface{})
	if !ok {
		err = errors.New("invalid structure inside db")
		return
	}

	for _, token := range tokens {
		_, ok = token.(string)
		if !ok {
			err = errors.New("invalid structure inside db")
			return
		}
	}

	old["tokens"] = append(tokens, token)
	var bytes []byte
	bytes, err = json.Marshal(old)
	if err != nil {
		return
	}

	err = ft.level.Put([]byte(ft.key), bytes, nil)
	return
}
