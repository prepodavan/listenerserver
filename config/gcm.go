package config

type GCM struct {
	ServerKey string `env:"GCM_SERVER_KEY"`
	APIKey    string `env:"GCM_API_KEY"`
}
