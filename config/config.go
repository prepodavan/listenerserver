package config

var Global = Config{
	GCM: &GCM{},
	Net: &Net{},
	Repos: &Repos{
		Tokens: &Tokens{},
	},
}

type Config struct {
	GCM   *GCM
	Net   *Net
	Repos *Repos
}
