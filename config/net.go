package config

type Net struct {
	Port int    `env:"PORT"`
	Host string `env:"HOST"`
}
