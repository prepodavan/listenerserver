package domain

type Post struct {
	ID     int    `json:"-" db:"id"`
	Title  string `json:"title" db:"title"`
	Posted int64  `json:"posted" db:"posted"`
	Link   string `json:"link" db:"link"`
}

type Observer func(newPost *Post)
