package reddit

import (
	"github.com/PuerkitoBio/goquery"
	"io"
	"rlis/domain"
	"strconv"
	"strings"
	"time"
)

type Extractor func(ts string) int64

var (
	extractors = []Extractor{minutes, hours, days, months, years}
)

func postsFromHtml(html io.Reader) ([]*domain.Post, error) {
	doc, err := goquery.NewDocumentFromReader(html)
	if err != nil {
		return nil, err
	}

	posts := make([]*domain.Post, 0)

	findTitles(doc).Each(func(i int, sel *goquery.Selection) {
		processTitleLink(&posts, sel, doc)
	})

	return posts, nil
}

func processNoTimestamp(doc *goquery.Document) {
	// ::NEEDS DB
	//html, err := doc.Html()
	//if err != nil {
	//	log.Println("cant get html; err: ", err)
	//} else {
	//	err = insertError(html)
	//	if err != nil {
	//		log.Println("cant insert err log: ", err)
	//	}
	//}
	//log.Println("missed both timestamps")
}

func processTitleLink(posts *[]*domain.Post, sel *goquery.Selection, doc *goquery.Document) {
	title := sel.Text()
	href, exists := sel.Attr("href")
	if !exists {
		return
	}

	ts := findTimestamp(sel)

	if ts == -1 {
		processNoTimestamp(doc)
	}

	p := &domain.Post{ID: -1, Title: title, Link: redditDomain + href, Posted: ts}
	*posts = append(*posts, p)
}

func findTitles(doc *goquery.Document) *goquery.Selection {
	return doc.Find("a[data-click-id*=body]")
}

func findTimestamp(titleLink *goquery.Selection) int64 {
	withID := findTimestampID(titleLink)
	if withID != nil {
		return timestampToUnix(*withID)
	}

	withoutID := findTimestampWithoutID(titleLink)
	if withoutID != nil {
		return timestampToUnix(*withoutID)
	}

	return -1
}

func findTimestampID(titleLink *goquery.Selection) *string {
	ts := titleLink.Parent().Parent().Prev().Find("a[data-click-id*=timestamp]")
	if ts.Size() < 1 {
		return nil
	}

	tsString := ts.First().Text()
	return &tsString
}

func findTimestampWithoutID(titleLink *goquery.Selection) *string {
	ts := titleLink.Parent().Parent().Prev().Find("a")
	if ts.Size() < 1 {
		return nil
	}

	possibleTs := ts.First().Next()
	if isTimestampNode(possibleTs) {
		tsString := possibleTs.Text()
		return &tsString
	}

	return nil
}

func isTimestampNode(sel *goquery.Selection) bool {
	return sel.Is("span")
}

func timestampToUnix(ts string) int64 {
	return extract(ts)
}

func extract(ts string) int64 {
	for _, extractor := range extractors {
		val := extractor(ts)
		if val != -1 {
			return val
		}
	}

	return -1
}

func minutes(ts string) int64 {
	return fromInterval(ts, "minute", func(diff int) time.Time {
		n := time.Now()
		return n.Add(-time.Duration(diff) * time.Minute)
	})
}

func hours(ts string) int64 {
	return fromInterval(ts, "hour", func(diff int) time.Time {
		n := time.Now()
		return n.Add(-time.Duration(diff) * time.Hour)
	})
}

func days(ts string) int64 {
	return fromInterval(ts, "day", func(diff int) time.Time {
		n := time.Now()
		return n.AddDate(0, 0, -diff)
	})
}

func months(ts string) int64 {
	return fromInterval(ts, "month", func(diff int) time.Time {
		n := time.Now()
		return n.AddDate(0, -diff, 0)
	})
}

func years(ts string) int64 {
	return fromInterval(ts, "year", func(diff int) time.Time {
		n := time.Now()
		return n.AddDate(-diff, 0, 0)
	})
}

func fromInterval(ts string, interval string, fromNow func(diff int) time.Time) int64 {
	if strings.Contains(ts, interval) {
		space := strings.Index(ts, " ")
		diff, err := strconv.Atoi(ts[:space])
		if err != nil {
			return -1
		}

		return fromNow(diff).Unix()
	}
	return -1
}
