package reddit

import (
	log "github.com/sirupsen/logrus"
	"net/http"
)

const (
	redditDomain = "https://www.reddit.com"
	agentHeader  = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 " +
		"(KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36"
	languageHeader = "en-US;q=0.8,en;q=0.7"
	charsetHeader  = "utf-8"
	encodingHeader = "identity"
	acceptHeader   = "*/*"
)

var (
	client = new(http.Client)
	subs   = make([]string, 0)
)

func getAllPosts() (responses []*http.Response, err error) {
	var res *http.Response
	for _, sub := range subs {
		res, err = getPosts(sub)
		if err != nil {
			log.
        	                WithFields(log.Fields{"tag":"log-res", "link": sub}).
                        	Error(log.Fields{"code": res.Status, "err": err.Error()})
			responses = nil
			return
		} else {
			log.
	                        WithFields(log.Fields{"tag":"log-res", "link": sub}).
	                        Debug(log.Fields{"code": res.Status})
		}

		responses = append(responses, res)
	}

	return
}

func getPosts(subUrl string) (*http.Response, error) {
	req, err := http.NewRequest("GET", subUrl, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Accept-Charset", charsetHeader)
	req.Header.Add("Accept-Language", languageHeader)
	req.Header.Add("Accept-Encoding", encodingHeader)
	req.Header.Add("Accept", acceptHeader)
	req.Header.Add("User-Agent", agentHeader)

	//for _, c := range cookies {
	//	req.AddCookie(c)
	//}

	return client.Do(req)
}
