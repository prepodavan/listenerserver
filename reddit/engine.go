package reddit

import (
	"net/http"
	log "github.com/sirupsen/logrus"
	domain "rlis/domain"
	"strings"
	"sync"
	"time"
)

const defaultDuration = 30 * time.Second

var (
	duration    = defaultDuration
	observers   = make([]domain.Observer, 0)
	shouldParse = true
	setOfLinks  = newSet()
)

func AddSub(subRedditsUrls ...string) {
	for _, sub := range subRedditsUrls {
		if len(sub) == 0 {
			continue
		}

		if !strings.HasPrefix(sub, "/") {
			sub = "/" + sub
		}

		if !strings.HasSuffix(sub, "/") {
			sub += "/"
		}

		subs = append(subs, redditDomain+sub)
	}
}

func AddObs(obs ...domain.Observer) {
	observers = append(observers, obs...)
}

func Stop() {
	shouldParse = false
}

func ParseForever(wg *sync.WaitGroup) {
	if setOfLinks == nil {
		setOfLinks = newSet()
	}

	if wg != nil {
		wg.Add(1)
		defer wg.Done()
		defer func() { setOfLinks = nil }()
	}

	for shouldParse {
		posts, err := getAndParse()
		if err != nil {
			continue
		}

		if wg != nil {
			wg.Add(1)
		}

		go func(posts []*domain.Post) {
			if wg != nil {
				wg.Done()
			}

			processOnlyNewPosts(posts)
		}(posts)

		time.Sleep(duration)
	}
}

func processOnlyNewPosts(posts []*domain.Post) {
	for _, post := range posts {
		log.
			WithFields(log.Fields{"tag": "log-post"}).
			Debug(post.Link)
		if setOfLinks.Contains(post.Link) {
			continue
		}

		log.
			WithFields(log.Fields{"tag": "log-new-post"}).
			Info(post.Link)

		setOfLinks.Add(post.Link)
		for _, obs := range observers {
			obs(post)
		}
	}
}

func setDurationByResponse(r *http.Response) {
	bannedDur := 25 * time.Minute
	if r.StatusCode == http.StatusOK {
		duration = defaultDuration
	} else if duration < bannedDur {
		duration = bannedDur
	}
}

func getAndParse() (allPosts []*domain.Post, err error) {
	var responses []*http.Response
	var posts []*domain.Post
	responses, err = getAllPosts()
	if err != nil {
		allPosts = nil
		return
	}

	for _, res := range responses {
		setDurationByResponse(res)
		posts, err = postsFromHtml(res.Body)
		if err != nil {
			allPosts = nil
			return
		}

		allPosts = append(allPosts, posts...)
	}

	return
}
