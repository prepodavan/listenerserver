package reddit

type postsSet struct {
	inner map[string]struct{}
}

func (s *postsSet) Contains(val string) bool {
	_, ok := s.inner[val]
	return ok
}

func (s *postsSet) Add(val string) {
	s.inner[val] = struct{}{}
}

func newSet() *postsSet {
	return &postsSet{inner: make(map[string]struct{})}
}
