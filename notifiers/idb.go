package notifiers

type TokensDB interface {
	GetAll() ([]string, error)
	Add(string) error
}
