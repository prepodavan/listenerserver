package notifiers

import (
	"container/list"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
	"net/http"
	"rlis/config"
	"rlis/domain"
)

type WSConnectionManager struct {
	conns    *list.List
	upgrader *websocket.Upgrader
}

func NewWSConnectionManager(conf *config.Config) (cm *WSConnectionManager, err error) {
	cm = &WSConnectionManager{
		conns: list.New(),
		upgrader: &websocket.Upgrader{
			ReadBufferSize:  1000024,
			WriteBufferSize: 1000024,
			CheckOrigin: func(_ *http.Request) bool {
				return true
			},
		},
	}

	return
}

func (cm *WSConnectionManager) Observer() domain.Observer {
	return cm.Notify
}

func (cm *WSConnectionManager) Handler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		conn, err := cm.upgrader.Upgrade(w, r, nil)
		if err != nil {
			return
		}

		el := cm.conns.PushBack(conn)
		defer func() {
			cm.conns.Remove(el)
			defer func() { recover() }()
			_ = conn.Close()
			recover()
		}()

		err = errors.New("stub")
		for err != nil {
			_, _, err = conn.ReadMessage()
		}
	}
}

func (cm *WSConnectionManager) GinHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		cm.Handler()(c.Writer, c.Request)
	}
}

func (cm *WSConnectionManager) Notify(newPost *domain.Post) {
	defer func() { recover() }()

	for el := cm.conns.Front(); el != nil; el = el.Next() {
		var ok bool
		var conn *websocket.Conn
		if conn, ok = el.Value.(*websocket.Conn); !ok {
			continue
		}

		_ = conn.WriteJSON(log.Fields{
			"version": "0.0.1",
			"event": log.Fields{
				"type": "new post",
			},
			"data": log.Fields{
				"post": newPost,
			},
		})
	}
}
