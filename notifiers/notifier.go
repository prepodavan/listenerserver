package notifiers

type Notifier func(interface{})
