package notifiers

import (
	"encoding/json"
	"github.com/appleboy/go-fcm"
	log "github.com/sirupsen/logrus"
	"rlis/config"
	"rlis/domain"
)

var (
	fcmClient *fcm.Client
)

func FCMObserver(repo TokensDB) domain.Observer {
	return func(newPost *domain.Post) {
		tokens, err := repo.GetAll()
		if err != nil {
			log.WithFields(log.Fields{
				"tag": "db: FirebaseTokens#GetAll err",
				"data": log.Fields{
					"dir_path": config.Global.Repos.Tokens.DirPath,
				},
			}).
				Error(err.Error())
		}

		msg := log.Fields{
			"version": "0.0.1",
			"event": log.Fields{
				"type": "new post",
			},
			"data": log.Fields{
				"post": newPost,
			},
		}

		NotifyWithFCM(msg, tokens...)
	}
}

func NotifyWithFCM(msg interface{}, tokens ...string) {
	bytes, err := json.Marshal(msg)
	if err != nil {
		log.WithFields(log.Fields{
			"tag": "notify: json err",
			"data": log.Fields{
				"msg": msg,
			},
		}).
			Error(err.Error())
		return
	}

	var jsonMap map[string]interface{}
	json.Unmarshal(bytes, &jsonMap)

	for _, user := range tokens {
		fcmMsg := &fcm.Message{
			To:   user,
			Data: jsonMap,
		}

		if _, err := fcmClient.Send(fcmMsg); err != nil {
			log.WithFields(log.Fields{
				"tag": "notify: fcm send err",
				"data": log.Fields{
					"to":  user,
					"msg": msg,
				},
			}).
				Error(err.Error())
		}
	}
}

func InitFCM() (err error) {
	fcmClient, err = fcm.NewClient(config.Global.GCM.APIKey)
	return
}
