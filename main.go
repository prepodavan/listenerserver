package main

import (
	"github.com/caarlos0/env"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
	"rlis/config"
	"rlis/notifiers"
	"rlis/reddit"
	"rlis/repos"
	"strconv"
	"sync"
	"time"
)

func main() {
	log.SetLevel(log.ErrorLevel)
	log.SetFormatter(&log.JSONFormatter{})
	check(env.Parse(&config.Global))

	wsConnectionManager, _ := notifiers.NewWSConnectionManager(&config.Global)
	check(notifiers.InitFCM())

	tokensRepo, err := repos.NewFirebaseTokens(config.Global.Repos.Tokens.DirPath)
	check(err)

	reddit.AddSub(os.Args[1:]...)
	reddit.AddObs(
		wsConnectionManager.Observer(),
		notifiers.FCMObserver(tokensRepo),
	)

	r := gin.New()
	r.Use(gin.Recovery())

	r.GET("/sub/:token", subHandler(tokensRepo))
	r.GET("/ws", wsConnectionManager.GinHandler())

	time.Sleep(1 * time.Second)
	wg := sync.WaitGroup{}
	go reddit.ParseForever(&wg)
	err = r.Run("0.0.0.0:" + strconv.Itoa(config.Global.Net.Port))
	reddit.Stop()
	wg.Wait()
	check(err)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func subHandler(repo *repos.FirebaseTokens) gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Param("token")
		old, err := repo.GetAll()
		if err != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		for _, t := range old {
			if t == token {
				c.Status(http.StatusOK)
				return
			}
		}

		err = repo.Add(token)
		if err != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		c.Status(http.StatusOK)
	}
}
