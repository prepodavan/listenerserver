module rlis

go 1.13

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/appleboy/go-fcm v0.1.5
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/gin-gonic/gin v1.6.0
	github.com/gorilla/websocket v1.4.2
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/syndtr/goleveldb v1.0.0
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
)
